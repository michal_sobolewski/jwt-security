package com.joonko.jwtsecurity.application.rest;

import com.joonko.jwtsecurity.domain.authorization.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@RequestMapping(value = "api/v1")
@RestController
public class ResourceController {

    @GetMapping(value = "resource/user")
    public Mono<ResponseEntity<?>> user() {
        log.info("resource for user");
        return Mono.just(ResponseEntity.ok(new Message("Content for admin")));
    }

    @GetMapping(value = "resource/admin")
    public Mono<ResponseEntity<?>> admin() {
        log.info("resource for admin");
        return Mono.just(ResponseEntity.ok(new Message("Content for admin")));
    }

}
